const modal = document.getElementById ('email-modal');
const openBtn = document.getElementById("myBtn")
const closeBtn = document.querySelector('.close-btn');


openBtn.addEventListener('click', () => {
   modal.style.display = 'block';

});

closeBtn.addEventListener('click', () =>{
    modal.style.display = 'none';
});

window.addEventListener('click', (e) => {
    if(e.target === modal) {
        modal.style.display='none';
    }
})
